FROM openjdk:11-jdk-slim
ADD ./jar/mycarhandler-1.0.jar my-car-handler.jar
CMD ["java", "-jar", "my-car-handler.jar"]