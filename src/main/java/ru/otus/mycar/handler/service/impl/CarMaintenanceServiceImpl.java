package ru.otus.mycar.handler.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.otus.mycar.handler.config.CarConfig;
import ru.otus.mycar.handler.dao.CarMaintenanceRepository;
import ru.otus.mycar.handler.model.CarMaintenance;
import ru.otus.mycar.handler.model.dto.CarMaintenanceDto;
import ru.otus.mycar.handler.model.enums.MaintenanceType;
import ru.otus.mycar.handler.service.CarMaintenanceService;
import ru.otus.mycar.handler.utils.CarMaintenanceMapper;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CarMaintenanceServiceImpl implements CarMaintenanceService {
    private final CarMaintenanceRepository repository;
    private final CarConfig config;
    private final CarMaintenanceMapper maintenanceMapper;

    @Override
    @Transactional
    public void save(CarMaintenanceDto carMaintenanceDto) {
        repository.save(processCarMaintenance(carMaintenanceDto));
    }

    @Override
    @Transactional(readOnly = true)
    public CarMaintenanceDto findByIdAndType(String userId, MaintenanceType maintenanceType) {
        CarMaintenance carMaintenance = getCarMaintenanceByUserIdAndType(userId, maintenanceType);
        return maintenanceMapper.convertFromEntityToDto(carMaintenance);
    }

    @Override
    public List<CarMaintenanceDto> findByUserId(String userId) {
        return repository.findByUserId(userId)
                         .stream()
                         .map(maintenanceMapper::convertFromEntityToDto)
                         .collect(Collectors.toList());
    }

    private CarMaintenance processCarMaintenance(CarMaintenanceDto carMaintenanceDto) {
        CarMaintenance carMaintenance = getCarMaintenanceByUserIdAndType(carMaintenanceDto.getUserId(), carMaintenanceDto.getMaintenanceType());
        return new CarMaintenance().setMaintenanceType(carMaintenance.getMaintenanceType())
                                   .setUserId(carMaintenance.getUserId())
                                   .setId(carMaintenance.getId())
                                   .setValue(carMaintenanceDto.getValue());
    }

    private CarMaintenance getCarMaintenanceByUserIdAndType(String userId, MaintenanceType maintenanceType) {
        return Optional.ofNullable(repository.findByUserIdAndType(userId, maintenanceType))
                       .orElse(new CarMaintenance().setValue(getValue(maintenanceType))
                                                   .setUserId(userId)
                                                   .setMaintenanceType(maintenanceType));
    }

    private int getValue(MaintenanceType maintenanceType) {
        switch (maintenanceType) {
            case TOTAL:
                return config.getTotal();
            case REMIND:
                return config.getReminder();
            case SCHEDULE:
                return config.getSchedule();
        }
        return 0;
    }


}
