package ru.otus.mycar.handler.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import ru.otus.mycar.handler.service.RequestService;

@Service
@RequiredArgsConstructor
public class WebClientService implements RequestService {
    private final WebClient webClient;

    @Override
    public void post(String uri) {
        webClient.post()
                 .uri(uri)
                 .contentType(MediaType.APPLICATION_JSON)
                 .retrieve()
                 .bodyToMono(String.class)
                 .block();
    }
}
