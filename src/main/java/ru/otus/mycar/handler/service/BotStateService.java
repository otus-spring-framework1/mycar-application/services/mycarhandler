package ru.otus.mycar.handler.service;

import ru.otus.mycar.handler.model.BotStateWrapper;

import java.util.List;

public interface BotStateService {
    void save(BotStateWrapper botStateWrapper);
    BotStateWrapper findById(Long id);
    List<BotStateWrapper> findAllBotStates();
}
