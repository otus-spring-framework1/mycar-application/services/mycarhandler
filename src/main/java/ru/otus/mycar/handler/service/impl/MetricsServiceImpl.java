package ru.otus.mycar.handler.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.otus.mycar.handler.dao.MetricsRepository;
import ru.otus.mycar.handler.model.Metrics;
import ru.otus.mycar.handler.model.dto.MetricsDto;
import ru.otus.mycar.handler.model.enums.MetricType;
import ru.otus.mycar.handler.service.MetricsService;
import ru.otus.mycar.handler.utils.MetricsMapper;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MetricsServiceImpl implements MetricsService {
    private final MetricsRepository metricsRepository;
    private final MetricsMapper metricsMapper;

    @Override
    @Transactional
    public void save(Metrics metrics) {
        metricsRepository.save(processMetrics(metrics));
    }

    @Override
    @Transactional(readOnly = true)
    public MetricsDto findByIdAndMetricType(String userId, MetricType metricType) {
        Metrics metrics = Optional.ofNullable(metricsRepository.findByUserIdAndMetricType(userId, metricType))
                                  .orElse(new Metrics().setValue("0")
                                                       .setDate(LocalDate.now())
                                                       .setId(userId)
                                                       .setMetricType(metricType));
        return metricsMapper.convertFromMetricsToMetricsDto(metrics);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Metrics> findAll() {
        return metricsRepository.findAll();
    }

    private Metrics processMetrics(Metrics metrics) {
        Metrics oldMetrics = Optional.ofNullable(metricsRepository.findByUserIdAndMetricType(metrics.getUserId(), metrics.getMetricType()))
                                     .orElse(new Metrics().setValue("0"));
        return new Metrics().setId(oldMetrics.getId())
                            .setUserId(metrics.getUserId())
                            .setMetricType(metrics.getMetricType())
                            .setDate(metrics.getDate())
                            .setValue(updateMetricsValue(metrics, oldMetrics));
    }

    private String updateMetricsValue(Metrics metrics, Metrics oldMetrics) {
        return metrics.getMetricType() == MetricType.MILEAGE
                ? metrics.getValue()
                : String.valueOf(Long.parseLong(metrics.getValue()) + Long.parseLong(oldMetrics.getValue()));
    }
}
