package ru.otus.mycar.handler.service;


public interface LanguageTranslatorService {
    String translateMessage(String message);
    void setCurrentLanguage(String language);
}
