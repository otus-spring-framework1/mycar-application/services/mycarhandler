package ru.otus.mycar.handler.service;

import ru.otus.mycar.handler.model.dto.CarMaintenanceDto;
import ru.otus.mycar.handler.model.enums.MaintenanceType;

import java.util.List;

public interface CarMaintenanceService {
    void save(CarMaintenanceDto carMaintenanceDto);
    CarMaintenanceDto findByIdAndType(String userId, MaintenanceType maintenanceType);
    List<CarMaintenanceDto> findByUserId(String id);
}
