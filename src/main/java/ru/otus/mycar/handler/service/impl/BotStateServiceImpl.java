package ru.otus.mycar.handler.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.otus.mycar.handler.dao.BotStateRepository;
import ru.otus.mycar.handler.model.BotStateWrapper;
import ru.otus.mycar.handler.model.enums.BotState;
import ru.otus.mycar.handler.service.BotStateService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BotStateServiceImpl implements BotStateService {
    private final BotStateRepository botStateRepository;

    @Override
    @Transactional
    public void save(BotStateWrapper botStateWrapper) {
        botStateRepository.save(botStateWrapper);
    }

    @Override
    @Transactional(readOnly = true)
    public BotStateWrapper findById(Long id) {
        return botStateRepository.findById(String.valueOf(id))
                                 .orElse(new BotStateWrapper().setBotState(BotState.INFORM));
    }

    @Override
    @Transactional(readOnly = true)
    public List<BotStateWrapper> findAllBotStates() {
        return botStateRepository.findAll();
    }
}
