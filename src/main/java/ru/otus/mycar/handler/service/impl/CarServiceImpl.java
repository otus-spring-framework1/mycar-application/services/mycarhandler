package ru.otus.mycar.handler.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.otus.mycar.handler.dao.CarRepository;
import ru.otus.mycar.handler.model.Car;
import ru.otus.mycar.handler.model.Metrics;
import ru.otus.mycar.handler.model.enums.MetricType;
import ru.otus.mycar.handler.service.CarService;
import ru.otus.mycar.handler.service.MetricsService;

import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CarServiceImpl implements CarService {
    private final CarRepository carRepository;
    private final MetricsService metricsService;

    @Override
    @Transactional
    public void save(Car car) {
        setInitialMileage(car);
        carRepository.save(car);
    }

    @Override
    @Transactional(readOnly = true)
    public Car findById(Long id) {
        return carRepository.findById(String.valueOf(id))
                            .orElse(new Car().setId("no car"));
    }

    @Override
    @Transactional(readOnly = true)
    public List<Car> findAllCars() {
        return carRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Car> findCarsByMileage(int from, int to) {
        return carRepository.findByMileageBetween(from, to);
    }

    private void setInitialMileage(Car car) {
        metricsService.save(new Metrics().setMetricType(MetricType.MILEAGE)
                                         .setDate(LocalDate.now())
                                         .setValue(String.valueOf(car.getMileage()))
                                         .setUserId(car.getId()));
    }
}
