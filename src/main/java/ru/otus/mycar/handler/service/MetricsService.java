package ru.otus.mycar.handler.service;

import ru.otus.mycar.handler.model.Metrics;
import ru.otus.mycar.handler.model.dto.MetricsDto;
import ru.otus.mycar.handler.model.enums.MetricType;

import java.util.List;

public interface MetricsService {
    void save(Metrics metrics);
    MetricsDto findByIdAndMetricType(String id, MetricType metricType);
    List<Metrics> findAll();
}
