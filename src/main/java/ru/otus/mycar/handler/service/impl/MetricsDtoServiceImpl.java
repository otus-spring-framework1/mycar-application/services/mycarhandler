package ru.otus.mycar.handler.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.otus.mycar.handler.model.Metrics;
import ru.otus.mycar.handler.model.dto.MetricsDto;
import ru.otus.mycar.handler.model.enums.MetricType;
import ru.otus.mycar.handler.service.CarService;
import ru.otus.mycar.handler.service.MetricsDtoService;
import ru.otus.mycar.handler.service.MetricsService;
import ru.otus.mycar.handler.utils.MetricsMapper;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MetricsDtoServiceImpl implements MetricsDtoService {
    private final CarService carService;
    private final MetricsService metricsService;
    private final MetricsMapper metricsMapper;


    @Override
    public void save(MetricsDto metricsDto) {
        Long userId = Long.parseLong(metricsDto.getUserId());
        Metrics metrics = metricsMapper.convertFromMetricsDtoToMetrics(metricsDto);
        if (metricsDto.getMetricType() == MetricType.MILEAGE) {
            Optional.ofNullable(carService.findById(userId))
                    .ifPresent(car -> {
                        car.setMileage(Integer.parseInt(metrics.getValue()));
                        carService.save(car);
                    });
        }
        metricsService.save(metrics);
    }
}
