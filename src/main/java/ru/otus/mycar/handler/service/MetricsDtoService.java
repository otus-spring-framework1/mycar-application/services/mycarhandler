package ru.otus.mycar.handler.service;

import ru.otus.mycar.handler.model.dto.MetricsDto;

public interface MetricsDtoService {
    void save(MetricsDto metricsDto);
}
