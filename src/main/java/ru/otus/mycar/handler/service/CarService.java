package ru.otus.mycar.handler.service;

import ru.otus.mycar.handler.model.Car;

import java.util.List;

public interface CarService {
    void save(Car car);
    Car findById(Long id);
    List<Car> findAllCars();
    List<Car> findCarsByMileage(int from, int to);
}
