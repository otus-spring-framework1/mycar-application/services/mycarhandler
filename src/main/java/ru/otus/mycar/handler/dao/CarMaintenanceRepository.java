package ru.otus.mycar.handler.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.otus.mycar.handler.model.CarMaintenance;
import ru.otus.mycar.handler.model.enums.MaintenanceType;

import java.util.List;

@Repository
public interface CarMaintenanceRepository extends MongoRepository<CarMaintenance, String> {
    @Query(value = "{$and: [{'userId' : :#{#userId}}, {'maintenanceType' : :#{#maintenanceType}}]}")
    CarMaintenance findByUserIdAndType(@Param("userId") String id, @Param("maintenanceType") MaintenanceType maintenanceType);
    List<CarMaintenance> findByUserId(String userId);
}
