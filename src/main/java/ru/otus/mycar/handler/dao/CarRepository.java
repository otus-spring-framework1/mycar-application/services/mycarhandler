package ru.otus.mycar.handler.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ru.otus.mycar.handler.model.Car;

import java.util.List;

@Repository
public interface CarRepository extends MongoRepository<Car,String> {
    List<Car> findByMileageBetween(int from, int to);
}
