package ru.otus.mycar.handler.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ru.otus.mycar.handler.model.BotStateWrapper;

@Repository
public interface BotStateRepository extends MongoRepository<BotStateWrapper, String> {
}
