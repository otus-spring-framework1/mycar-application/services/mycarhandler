package ru.otus.mycar.handler.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.otus.mycar.handler.model.Metrics;
import ru.otus.mycar.handler.model.enums.MetricType;

@Repository
public interface MetricsRepository extends MongoRepository<Metrics, String> {
    @Query(value = "{$and: [{'userId' : :#{#userId}}, {'metricType' : :#{#metricType}}]}")
    Metrics findByUserIdAndMetricType(@Param("userId") String id, @Param("metricType")MetricType metricType);
}
