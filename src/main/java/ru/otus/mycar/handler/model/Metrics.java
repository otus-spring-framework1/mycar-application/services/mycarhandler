package ru.otus.mycar.handler.model;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import ru.otus.mycar.handler.model.enums.MetricType;

import java.time.LocalDate;

@Data
@Document(collection = "Metrics")
@Accessors(chain = true)
public class Metrics {
    @Id
    private String id;
    private String userId;
    private MetricType metricType;
    private String value;
    private LocalDate date;
}
