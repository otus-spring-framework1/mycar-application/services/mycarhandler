package ru.otus.mycar.handler.model;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import ru.otus.mycar.handler.model.enums.MaintenanceType;

@Data
@Document(collection = "CarMaintenance")
@Accessors(chain = true)
public class CarMaintenance {
    @Id
    private String id;
    private String userId;
    private int value;
    private MaintenanceType maintenanceType;

}
