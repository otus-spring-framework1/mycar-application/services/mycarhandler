package ru.otus.mycar.handler.model;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "Car")
@Accessors(chain = true)
public class Car {
    @Id
    private String id;
    private String chatId;
    private String model;
    private int year;
    private float engineVolume;
    private String transmission;
    private int mileage;
}
