package ru.otus.mycar.handler.model.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import ru.otus.mycar.handler.model.enums.MaintenanceType;

@Data
@Accessors(chain = true)
public class CarMaintenanceDto {
    private String userId;
    private int value;
    private MaintenanceType maintenanceType;
}
