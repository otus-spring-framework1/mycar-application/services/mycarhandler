package ru.otus.mycar.handler.model.enums;

public enum MetricType {
    MILEAGE, GAS, SPARES;
}
