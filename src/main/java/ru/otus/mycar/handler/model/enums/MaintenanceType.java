package ru.otus.mycar.handler.model.enums;

public enum MaintenanceType {
    TOTAL, REMIND, SCHEDULE
}
