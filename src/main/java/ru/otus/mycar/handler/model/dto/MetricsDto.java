package ru.otus.mycar.handler.model.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import ru.otus.mycar.handler.model.enums.MetricType;

import java.time.LocalDate;

@Data
@Accessors(chain = true)
public class MetricsDto {
    private String userId;
    private MetricType metricType;
    private String value;
    private LocalDate date;
}


