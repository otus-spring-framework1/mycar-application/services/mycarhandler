package ru.otus.mycar.handler.model;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import ru.otus.mycar.handler.model.enums.BotState;

@Data
@Document(collection = "BotState")
@Accessors(chain = true)
public class BotStateWrapper {
    @Id
    private String id;
    private BotState botState;
}
