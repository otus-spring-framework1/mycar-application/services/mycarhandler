package ru.otus.mycar.handler.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
public class TelegramBotConfig {
    @Value("${bot.telegram-api}")
    private String telegramApi;
    @Value("${bot.token}")
    private String token;
}
