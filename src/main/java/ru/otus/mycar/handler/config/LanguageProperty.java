package ru.otus.mycar.handler.config;


import org.springframework.stereotype.Component;

import java.util.Locale;

@Component
public class LanguageProperty {
    private String language = "ru";
    public String getCurrentLanguage() {
        return new Locale(language,language.toUpperCase()).getLanguage();
    }
    public void setLanguage(String language) {
        this.language = language;
    }
}
