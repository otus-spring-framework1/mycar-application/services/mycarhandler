package ru.otus.mycar.handler.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.otus.mycar.handler.model.Metrics;
import ru.otus.mycar.handler.model.dto.MetricsDto;
import ru.otus.mycar.handler.model.enums.MetricType;
import ru.otus.mycar.handler.service.MetricsDtoService;
import ru.otus.mycar.handler.service.MetricsService;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class MetricsController {
    private final MetricsService metricsService;
    private final MetricsDtoService metricsDtoService;

    @PostMapping(value = "${metrics.api.baseurl}")
    public ResponseEntity<Object> saveCosts(@RequestBody MetricsDto metricsDto) {
        metricsDtoService.save(metricsDto);
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "${metrics.api.sub-path.metric-type}")
    public ResponseEntity<MetricsDto> getCostsById(@PathVariable("id") Long id,
                                                @PathVariable("metricType")MetricType metricType){
        return ResponseEntity.ok(metricsService.findByIdAndMetricType(String.valueOf(id), metricType));
    }

    @GetMapping(value = "${metrics.api.baseurl}")
    public ResponseEntity<List<Metrics>> getAll(){
        return ResponseEntity.ok(metricsService.findAll());
    }
}
