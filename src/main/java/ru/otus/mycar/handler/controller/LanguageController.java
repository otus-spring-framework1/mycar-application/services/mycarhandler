package ru.otus.mycar.handler.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.otus.mycar.handler.service.LanguageTranslatorService;

@RestController
@RequiredArgsConstructor
public class LanguageController {
    private final LanguageTranslatorService languageTranslatorService;

    @PostMapping(value = "${language.api.baseurl}")
    public ResponseEntity getCurrentLanguage(@RequestBody String language){
        languageTranslatorService.setCurrentLanguage(language);
        return ResponseEntity.ok().build();
    }
}
