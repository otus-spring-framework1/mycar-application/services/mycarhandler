package ru.otus.mycar.handler.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.otus.mycar.handler.model.BotStateWrapper;
import ru.otus.mycar.handler.service.BotStateService;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class BotStateController {
    private final BotStateService botStateService;

    @PostMapping(value = "${bot-state.api.baseurl}")
    public ResponseEntity<Object> saveBotState(@RequestBody BotStateWrapper botStateWrapper) {
        botStateService.save(botStateWrapper);
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "${bot-state.api.sub-path.id}")
    public ResponseEntity<BotStateWrapper> getBotStateById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(botStateService.findById(id));
    }

    @GetMapping(value = "${bot-state.api.sub-path.all}")
    public ResponseEntity<List<BotStateWrapper>> getBotStateList() {
        return ResponseEntity.ok(botStateService.findAllBotStates());
    }
}
