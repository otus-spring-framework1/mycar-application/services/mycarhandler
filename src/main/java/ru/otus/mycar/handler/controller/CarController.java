package ru.otus.mycar.handler.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.otus.mycar.handler.model.Car;
import ru.otus.mycar.handler.service.CarService;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class CarController {
    private final CarService carService;

    @PostMapping(value = "${car.api.baseurl}")
    public ResponseEntity<Object> saveCar(@RequestBody Car car) {
        carService.save(car);
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "${car.api.sub-path.id}")
    public ResponseEntity<Car> getCarById(@PathVariable("id") Long id){
        return ResponseEntity.ok(carService.findById(id));
    }

    @GetMapping(value = "${car.api.sub-path.all}")
    public ResponseEntity<List<Car>> getCarList(){
        return ResponseEntity.ok(carService.findAllCars());
    }
}