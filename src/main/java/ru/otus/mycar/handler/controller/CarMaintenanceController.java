package ru.otus.mycar.handler.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.otus.mycar.handler.model.dto.CarMaintenanceDto;
import ru.otus.mycar.handler.service.CarMaintenanceService;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class CarMaintenanceController {
    private final CarMaintenanceService carMaintenanceService;

    @PostMapping(value = "${car-maintenance.api.baseurl}")
    public ResponseEntity<Object> saveCarMaintenance(@RequestBody CarMaintenanceDto carMaintenanceDto) {
        carMaintenanceService.save(carMaintenanceDto);
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "${car-maintenance.api.sub-path.id}")
    public ResponseEntity<List<CarMaintenanceDto>> getCarMaintenanceById(@PathVariable("id") Long id){
        return ResponseEntity.ok(carMaintenanceService.findByUserId(String.valueOf(id)));
    }
}
