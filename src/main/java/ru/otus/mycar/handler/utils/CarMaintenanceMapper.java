package ru.otus.mycar.handler.utils;

import org.springframework.stereotype.Component;
import ru.otus.mycar.handler.model.CarMaintenance;
import ru.otus.mycar.handler.model.dto.CarMaintenanceDto;

@Component
public class CarMaintenanceMapper {

    public CarMaintenanceDto convertFromEntityToDto(CarMaintenance carMaintenance) {
        return new CarMaintenanceDto().setUserId(carMaintenance.getUserId())
                                      .setMaintenanceType(carMaintenance.getMaintenanceType())
                                      .setValue(carMaintenance.getValue());
    }

    public CarMaintenance convertFromDtoToEntity(CarMaintenanceDto carMaintenanceDto) {
        return new CarMaintenance().setUserId(carMaintenanceDto.getUserId())
                                   .setMaintenanceType(carMaintenanceDto.getMaintenanceType())
                                   .setValue(carMaintenanceDto.getValue());
    }
}
