package ru.otus.mycar.handler.utils;

import org.springframework.stereotype.Component;
import ru.otus.mycar.handler.model.Metrics;
import ru.otus.mycar.handler.model.dto.MetricsDto;

@Component
public class MetricsMapper {

    public MetricsDto convertFromMetricsToMetricsDto(Metrics metrics) {
        return new MetricsDto().setUserId(metrics.getUserId())
                               .setMetricType(metrics.getMetricType())
                               .setDate(metrics.getDate())
                               .setValue(metrics.getValue());
    }

    public Metrics convertFromMetricsDtoToMetrics(MetricsDto metricsDto) {
        return new Metrics().setUserId(metricsDto.getUserId())
                            .setMetricType(metricsDto.getMetricType())
                            .setDate(metricsDto.getDate())
                            .setValue(metricsDto.getValue());
    }
}
