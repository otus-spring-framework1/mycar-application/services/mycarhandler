package ru.otus.mycar.handler.utils;

import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.otus.mycar.handler.config.TelegramBotConfig;
import ru.otus.mycar.handler.model.Car;
import ru.otus.mycar.handler.model.enums.MaintenanceType;
import ru.otus.mycar.handler.service.CarMaintenanceService;
import ru.otus.mycar.handler.service.CarService;
import ru.otus.mycar.handler.service.LanguageTranslatorService;
import ru.otus.mycar.handler.service.RequestService;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
@EnableScheduling
public class ScheduledMessageSender {
    private final RequestService requestService;
    private final TelegramBotConfig telegramBotConfig;
    private final CarService carService;
    private final LanguageTranslatorService translator;
    private final CarMaintenanceService carMaintenanceService;


    @Scheduled(fixedDelayString = "${schedule.delay.fixed}", initialDelayString = "${schedule.delay.init}")
    public void sendMessageToTelegramBot() {
        getCarsWithServiceMileage().forEach(car -> {
            String urlString = configureUrl(translator.translateMessage("car.maintenance"), car.getChatId());
            requestService.post(urlString);
        });
    }

    private String configureUrl(String text, String chatId) {
        String urlString = telegramBotConfig.getTelegramApi();
        String apiToken = telegramBotConfig.getToken();
        return String.format(urlString, apiToken, chatId, text);
    }

    public List<Car> getCarsWithServiceMileage() {
        List<Car> usersPlusWorkList = new ArrayList<>();
        List<Car> carList = carService.findAllCars();
        carList.forEach(car -> {
            int scheduledValue = getMaintenanceTypeValue(car, MaintenanceType.SCHEDULE);
            int totalValue = getMaintenanceTypeValue(car, MaintenanceType.TOTAL);
            int remindValue = getMaintenanceTypeValue(car, MaintenanceType.REMIND);

            for (int i = scheduledValue; i < totalValue; i = i + scheduledValue) {
                usersPlusWorkList.addAll(carService.findCarsByMileage(i - remindValue, i));
            }
        });
        return usersPlusWorkList;
    }

    private int getMaintenanceTypeValue(Car car, MaintenanceType type) {
        return carMaintenanceService.findByIdAndType(car.getId(), type)
                                    .getValue();
    }
}
