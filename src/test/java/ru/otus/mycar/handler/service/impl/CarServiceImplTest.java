package ru.otus.mycar.handler.service.impl;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import ru.otus.mycar.handler.dao.CarRepository;
import ru.otus.mycar.handler.model.Car;
import ru.otus.mycar.handler.service.CarService;
import ru.otus.mycar.handler.service.MetricsService;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
@ContextConfiguration(classes = CarServiceImpl.class)
class CarServiceImplTest {
    @MockBean
    private CarRepository carRepository;
    @MockBean
    private MetricsService metricsService;

    @Autowired
    private CarService carService;

    private static final String ID = "1";

    @Test
    void findById() {
        var car = new Car().setId(ID);
        when(carRepository.findById(ID)).thenReturn(Optional.of(car));
        assertEquals(car, carService.findById(Long.parseLong(ID)));
    }
}