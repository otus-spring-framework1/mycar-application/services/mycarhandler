package ru.otus.mycar.handler.service.impl;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import ru.otus.mycar.handler.dao.BotStateRepository;
import ru.otus.mycar.handler.model.BotStateWrapper;
import ru.otus.mycar.handler.service.BotStateService;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
@ContextConfiguration(classes = BotStateServiceImpl.class)
class BotStateServiceImplTest {

    @MockBean
    private BotStateRepository botStateRepository;
    @Autowired
    private BotStateService botStateService;

    private static final String ID = "1";

    @Test
    void findById() {
        var botStateWrapper = Optional.of(new BotStateWrapper().setId(ID));
        when(botStateRepository.findById(ID)).thenReturn(botStateWrapper);
        assertEquals(botStateWrapper.get(), botStateService.findById(Long.parseLong(ID)));
    }
}